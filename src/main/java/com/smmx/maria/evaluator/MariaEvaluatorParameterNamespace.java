/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.evaluator;

/**
 * @author Osvaldo Miguel Colin
 */
public abstract class MariaEvaluatorParameterNamespace {

    private final String name;
    private final boolean is_temporary;

    public MariaEvaluatorParameterNamespace(String name, boolean is_temporary) {
        this.name = name;
        this.is_temporary = is_temporary;
    }

    public String getName() {
        return name;
    }

    public boolean isTemporary() {
        return is_temporary;
    }

    public abstract boolean exists(String parameter);

    public abstract Object get(String parameter);

    public abstract Object pop(String parameter);
}
