/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.evaluator;

/**
 * @author Osvaldo Miguel Colin
 */
public class MariaEvaluatorException extends Exception {

    public MariaEvaluatorException(Throwable thrwbl) {
        super(thrwbl);
    }

    public MariaEvaluatorException(String msg) {
        super(msg);
    }

}
