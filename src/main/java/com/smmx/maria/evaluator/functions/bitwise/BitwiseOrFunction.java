/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.evaluator.functions.bitwise;


import com.smmx.maria.evaluator.MariaEvaluatorFunction;
import net.sourceforge.jeval.function.FunctionException;

import java.util.List;

import static com.smmx.maria.commons.MariaCommons.lp;


/**
 * @author Osvaldo Miguel Colin
 */
public class BitwiseOrFunction extends MariaEvaluatorFunction {

    public BitwiseOrFunction() {
        super("binOr");
    }

    @Override
    public Object apply(List<Object> args) throws FunctionException {
        int a = lp()
            .require(0)
            .asInteger()
            .notNull()
            .apply(args);

        int b = lp()
            .require(1)
            .asInteger()
            .notNull()
            .apply(args);

        return a | b;
    }
}
