/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.evaluator.functions.strings;


import com.smmx.maria.evaluator.MariaEvaluatorFunction;
import net.sourceforge.jeval.function.FunctionException;

import java.util.List;

import static com.smmx.maria.commons.MariaCommons.lp;

/**
 * @author Osvaldo Miguel Colin
 */
public class ConcatFunction extends MariaEvaluatorFunction {

    public ConcatFunction() {
        super("concatenate");
    }

    @Override
    public Object apply(List<Object> args) throws FunctionException {
        // CHECKS
        if (args.size() < 2) {
            throw new FunctionException("Function takes two or more arguments.");
        }

        // NEW BUILDER
        StringBuilder string_builder = new StringBuilder();

        // GET DELIMITER
        String delimiter = lp()
            .require(0)
            .asString()
            .notNull()
            .apply(args);

        // FOR EACH ITEM
        for (int i = 1; i < args.size(); i++) {
            // GET ARG
            String item = lp()
                .require(i)
                .asString()
                .notNull()
                .apply(args);

            // USE COMMA?
            if (i > 1) {
                string_builder.append(delimiter);
            }

            // APPEND
            string_builder.append(item);
        }

        // TO STR
        return string_builder.toString();
    }

}
