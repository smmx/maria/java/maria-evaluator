/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.evaluator.functions.strings;


import com.smmx.maria.evaluator.MariaEvaluatorFunction;
import net.sourceforge.jeval.function.FunctionException;

import java.util.List;

import static com.smmx.maria.commons.MariaCommons.lp;

/**
 * @author Osvaldo Miguel Colin
 */
public class FormatFunction extends MariaEvaluatorFunction {

    public FormatFunction() {
        super("format");
    }

    @Override
    public Object apply(List<Object> args) throws FunctionException {
        // CHECKS
        if (args.size() < 2) {
            throw new FunctionException("Function takes two or more arguments.");
        }

        // GET FORMAT
        String format = lp()
            .require(0)
            .asString()
            .notNull()
            .apply(args);

        // GET ARGS ARRAY
        Object[] array = new Object[args.size() - 1];

        for (int i = 1; i < args.size(); i++) {
            array[i - 1] = lp()
                .require(i)
                .asString()
                .notNull()
                .apply(args);
        }

        // FORMAT
        return String.format(format, array);
    }

}
