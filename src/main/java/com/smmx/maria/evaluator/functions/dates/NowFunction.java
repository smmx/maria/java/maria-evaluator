/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.evaluator.functions.dates;


import com.smmx.maria.evaluator.MariaEvaluatorFunction;
import net.sourceforge.jeval.function.FunctionException;

import java.util.Calendar;
import java.util.List;

/**
 * @author Osvaldo Miguel Colin
 */
public class NowFunction extends MariaEvaluatorFunction {

    public NowFunction() {
        super("now");
    }

    @Override
    public Object apply(List<Object> args) throws FunctionException {
        return ((double) Calendar.getInstance().getTime().getTime()) / 1000d;
    }

}
