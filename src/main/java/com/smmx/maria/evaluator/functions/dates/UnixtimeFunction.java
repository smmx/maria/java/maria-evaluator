/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.evaluator.functions.dates;


import com.smmx.maria.evaluator.MariaEvaluatorFunction;
import net.sourceforge.jeval.function.FunctionException;

import java.util.List;

import static com.smmx.maria.commons.MariaCommons.lp;


/**
 * @author Osvaldo Miguel Colin
 */
public class UnixtimeFunction extends MariaEvaluatorFunction {

    public UnixtimeFunction() {
        super("unixtime");
    }

    @Override
    public Object apply(List<Object> args) throws FunctionException {

        return lp()
            .require(0)
            .asDate()
            .map(d -> ((double) d.getTime()) / 1000d)
            .apply(args);
    }

}
