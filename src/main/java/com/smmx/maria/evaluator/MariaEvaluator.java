/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.evaluator;

import com.smmx.maria.evaluator.functions.bitwise.*;
import com.smmx.maria.evaluator.functions.dates.NowFunction;
import com.smmx.maria.evaluator.functions.dates.UnixtimeFunction;
import com.smmx.maria.evaluator.functions.strings.FormatFunction;
import com.smmx.maria.evaluator.functions.strings.ConcatFunction;
import net.sourceforge.jeval.ArgumentTokenizer;
import net.sourceforge.jeval.EvaluationConstants;
import net.sourceforge.jeval.EvaluationException;
import net.sourceforge.jeval.Evaluator;
import net.sourceforge.jeval.function.Function;
import net.sourceforge.jeval.function.FunctionConstants;
import net.sourceforge.jeval.function.FunctionException;
import net.sourceforge.jeval.function.FunctionResult;
import org.apache.commons.lang3.StringUtils;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.smmx.maria.commons.MariaCommons.uuid;

/**
 * @author Osvaldo Miguel Colin
 */
public class MariaEvaluator extends Evaluator {

    // STATIC
    public static final Pattern PARAMETER_REGEX = Pattern.compile("\\$(\\w*)\\{\\s*([^,}]+)\\s*((?:,\\s*[^,}]+\\s*)*)\\s*}");
    public static final Pattern ARG_SEPARATOR_REGEX = Pattern.compile(",(?=(?:'(?:\\\\'|[^'])+'|[^,']+)(?:,|$))");

    // MEMBER
    private final List<Function> functions;
    private final Map<String, MariaEvaluatorParameterNamespace> parameter_namespaces;
    private final Map<String, Object> temporary_results;
    private final Map<String, Object> arguments;

    public MariaEvaluator() {
        this.functions = new ArrayList<>();
        this.parameter_namespaces = new HashMap<>();
        this.temporary_results = new HashMap<>();
        this.arguments = new HashMap<>();

        // SETUP
        registerParameterNamespace(new MariaEvaluatorParameterNamespace("tmp", true) {
            @Override
            public boolean exists(String parameter) {
                return temporary_results.containsKey(parameter);
            }

            @Override
            public Object get(String parameter) {
                return temporary_results.get(parameter);
            }

            @Override
            public Object pop(String parameter) {
                return temporary_results.remove(parameter);
            }
        });

        // BASIC FUNCTIONS
        putFunction(new BitwiseAndFunction());
        putFunction(new BitwiseOrFunction());
        putFunction(new BitwiseXorFunction());
        putFunction(new BitwiseShiftLeftFunction());
        putFunction(new BitwiseShiftRightFunction());

        putFunction(new FormatFunction());
        putFunction(new ConcatFunction());

        putFunction(new NowFunction());
        putFunction(new UnixtimeFunction());
    }

    // SETUP
    private Map<String, MariaEvaluatorParameterNamespace> getParameterNamespaces() {
        return parameter_namespaces;
    }

    private void setParameterNamespaces(Map<String, MariaEvaluatorParameterNamespace> namespaces) {
        this.parameter_namespaces.clear();
        this.parameter_namespaces.putAll(namespaces);
    }

    private Map<String, Object> getTemporaryResults() {
        return temporary_results;
    }

    private void setTemporaryResults(Map<String, Object> temporary_results) {
        this.temporary_results.clear();
        this.temporary_results.putAll(temporary_results);
    }

    private Map<String, Object> getArguments() {
        return arguments;
    }

    private void setArguments(Map<String, Object> execution_parameters) {
        this.arguments.clear();
        this.arguments.putAll(execution_parameters);
    }

    // PUTS
    @Override
    public void removeFunction(String function_name) {
        super.removeFunction(function_name);

        // LOOK UP
        Function function_to_be_removed = null;

        for (Function a_function : functions) {
            if (function_name.equals(a_function.getName())) {
                function_to_be_removed = a_function;
                break;
            }
        }

        // REMOVE
        if (function_to_be_removed != null) {
            functions.remove(function_to_be_removed);
        }
    }

    @Override
    public void putFunction(Function function) {
        super.putFunction(function);

        // ADD
        if (functions != null) {
            functions.add(function);
        }
    }

    public void putFunction(MariaEvaluatorFunction function) {
        putFunction(new MariaEvaluatorFunctionWrapper(function));
    }

    public void removeParameterNamespace(String parameter_storage) {
        parameter_namespaces.remove(parameter_storage);
    }

    public void registerParameterNamespace(MariaEvaluatorParameterNamespace namespace) {
        parameter_namespaces.put(namespace.getName(), namespace);
    }

    // OVERRIDE
    @Override
    protected String processNestedFunctions(String arguments) throws EvaluationException {
        StringBuilder evaluatedArguments = new StringBuilder();

        // Process nested function calls.
        if (arguments.length() > 0) {
            com.smmx.maria.evaluator.MariaEvaluator argumentsEvaluator = new com.smmx.maria.evaluator.MariaEvaluator();

            argumentsEvaluator.setFunctions(getFunctions());
            argumentsEvaluator.setVariables(getVariables());
            argumentsEvaluator.setVariableResolver(getVariableResolver());
            argumentsEvaluator.setTemporaryResults(getTemporaryResults());
            argumentsEvaluator.setArguments(getArguments());
            argumentsEvaluator.setParameterNamespaces(getParameterNamespaces());

            final ArgumentTokenizer tokenizer = new ArgumentTokenizer(arguments, EvaluationConstants.FUNCTION_ARGUMENT_SEPARATOR);

            List<Object> evalautedArgumentList = new ArrayList<>();

            while (tokenizer.hasMoreTokens()) {
                String argument = tokenizer.nextToken().trim();

                try {
                    argument = argumentsEvaluator.evaluate(argument);
                } catch (EvaluationException e) {

                }

                evalautedArgumentList.add(argument);
            }

            for (Object arg : evalautedArgumentList) {
                if (evaluatedArguments.length() > 0) {
                    evaluatedArguments.append(EvaluationConstants.FUNCTION_ARGUMENT_SEPARATOR);
                }

                String evaluatedArgument = (String) arg;
                evaluatedArguments.append(evaluatedArgument);
            }
        }

        return evaluatedArguments.toString();
    }

    // EVALUATE
    public Object evaluate(String expression, Map<String, Object> arguments) throws MariaEvaluatorException {
        if (expression == null) {
            throw new IllegalArgumentException("Expression can't be null.");
        }

        if (arguments == null) {
            throw new IllegalArgumentException("Arguments can't be null.");
        }

        try {
            // SET ARGUMENTS
            setArguments(arguments);

            // REPLACE PARAMETERS
            expression = resolveParameters(expression, false);

            // EVAL
            String result_str = evaluate(expression);

            // SHORT-CIRCUIT
            if (result_str == null) {
                throw new MariaEvaluatorException(
                    String.format("Invalid expression: \"%s\".", expression)
                );
            }

            // RETURN
            if (result_str.startsWith("'") && result_str.endsWith("'")) {
                // IF IT'S A STRING, STRIP QUOTES
                result_str = StringUtils.strip(result_str, "'");

                // CHECK IF IT'S A PARAMETER
                if (PARAMETER_REGEX.matcher(result_str).matches()) {
                    return evaluateParameter(result_str);
                }

                // REPLACE ANY PARAMETER INSIDE
                return resolveParameters(result_str, true);
            } else {
                // RETURN IF IT'S NOT A STRING
                return result_str;
            }
        } catch (EvaluationException ex) {
            throw new MariaEvaluatorException(ex);
        } finally {
            this.arguments.clear();
        }
    }

    private String resolveParameters(String expression, boolean is_final) throws MariaEvaluatorException {
        // CREATE REPLACEMENT
        StringBuffer result_buffer = new StringBuffer();

        // FIND PARAMETERS
        Matcher matcher = PARAMETER_REGEX.matcher(expression);

        // REPLACE PARAMETERS
        while (matcher.find()) {
            String storage = matcher.group(1);
            String name = matcher.group(2);

            // GET VALUE
            Object value = getParameter(storage, name);

            // FIND REPLACEMENT
            String replacement;

            if (is_final) {
                replacement = Objects.toString(value);
            } else {
                replacement = String.format("$%s{%s}", storage, name);

                if (value instanceof Number) {
                    replacement = value.toString();
                } else if (value instanceof String) {
                    replacement = "'" + value.toString() + "'";
                } else if (value instanceof Boolean) {
                    replacement = ((boolean) value) ? "1.0" : "0.0";
                }
            }

            // QUOTE
            replacement = Matcher.quoteReplacement(replacement);

            // REPLACE
            matcher.appendReplacement(result_buffer, replacement);
        }

        // APPEND TAIL
        matcher.appendTail(result_buffer);

        // RETURN
        return result_buffer.toString();
    }

    private Object evaluateParameter(String expression) throws MariaEvaluatorException {
        // MATCH
        Matcher matcher = PARAMETER_REGEX.matcher(expression);

        // IS PARAMETER?
        if (matcher.matches()) {
            String parameter_storage = matcher.group(1);
            String parameter_name = matcher.group(2);

            // GET VALUE
            return getParameter(parameter_storage, parameter_name);
        }

        throw new MariaEvaluatorException(String.format("Expression \"%s\" is not a parameter.", expression));
    }

    private Object getParameter(String namespace_name, String parameter_name) throws MariaEvaluatorException {
        boolean no_namespace = StringUtils.isEmpty(namespace_name);

        if (no_namespace) {
            // EXISTS?
            if (!arguments.containsKey(parameter_name)) {
                throw new MariaEvaluatorException(String.format("Parameter \"%s\" doesn't exist.", parameter_name));
            }

            // NO NAMESPACE USE ARGS
            return arguments.get(parameter_name);
        } else {
            // GET NAMESPACE
            MariaEvaluatorParameterNamespace namespace = parameter_namespaces.get(namespace_name);

            // CHECK NAMESPACE
            if (namespace == null) {
                throw new MariaEvaluatorException(String.format("Namespace \"%s\" doesn't exist.", namespace_name));
            }

            // EXISTS?
            if (!namespace.exists(parameter_name)) {
                throw new MariaEvaluatorException(String.format("Namespace \"%s\" doesn't contain a parameter named \"%s\".", namespace_name, parameter_name));
            }

            // POP IF IT'S TEMPORARY
            if (namespace.isTemporary()) {
                return namespace.pop(parameter_name);
            }

            // GET
            return namespace.get(parameter_name);
        }
    }

    // CLASSES
    private class MariaEvaluatorFunctionWrapper implements Function {

        private final MariaEvaluatorFunction function;

        public MariaEvaluatorFunctionWrapper(MariaEvaluatorFunction function) {
            this.function = function;
        }

        @Override
        public String getName() {
            return function.getName();
        }

        @Override
        public FunctionResult execute(Evaluator evaluator, String args_string) throws FunctionException {
            // CHECK
            if (!(evaluator instanceof MariaEvaluator)) {
                throw new FunctionException("Evaluator is not an Maria Evaluator!");
            }

            // GET PARAMETERS
            List<Object> parameters = new ArrayList<>();

            if (!args_string.isEmpty()) {
                // UNPACK ARGS
                String[] arg_strings = ARG_SEPARATOR_REGEX.split(args_string);

                // FOR EACH ARG
                for (String arg_string : arg_strings) {
                    // REMOVE SINGLE QUOTES
                    arg_string = StringUtils.strip(arg_string, "'");

                    // GET VALUE
                    Object arg_value = arg_string;

                    // CHECK IF IT'S A PARAMETER
                    if (PARAMETER_REGEX.matcher(arg_string).matches()) {
                        try {
                            arg_value = evaluateParameter(arg_string);
                        } catch (MariaEvaluatorException e) {
                            throw new FunctionException(e);
                        }
                    }

                    // RETURN
                    parameters.add(arg_value);
                }
            }

            // CONST PARAMETERS
            parameters = Collections.unmodifiableList(parameters);

            // APPLY
            Object result = function.apply(parameters);

            // RETURN
            return result(result);
        }

        private FunctionResult result(Object result) throws FunctionException {
            if (result instanceof Number) {
                // CAST
                Number num_result = (Number) result;

                // RETURN
                return new FunctionResult(num_result.toString(), FunctionConstants.FUNCTION_RESULT_TYPE_NUMERIC);
            } else if (result instanceof Boolean) {
                // CAST
                boolean bool_result = (boolean) result;

                // REPLACE WITH
                String replacement = bool_result ? "1.0" : "0.0";

                // RETURN
                return new FunctionResult(replacement, FunctionConstants.FUNCTION_RESULT_TYPE_NUMERIC);
            } else if (result instanceof String) {
                // CAST
                String str_result = (String) result;

                // RETURN
                return new FunctionResult(str_result, FunctionConstants.FUNCTION_RESULT_TYPE_STRING);
            }

            // IF VALUE IS NOT A NUMBER, BOOLEAN OR STRING USE EXPLICIT VALUE
            String tmp = temporary(result);

            // RETURN TEMP VARIABLE
            return new FunctionResult(tmp, FunctionConstants.FUNCTION_RESULT_TYPE_STRING);
        }

        private String temporary(Object result) {
            // GENERATE UUID
            String uuid = uuid();

            // STORE
            temporary_results.put(uuid, result);

            // RETURN VAR
            return String.format("$tmp{%s}", uuid);
        }
    }

}
