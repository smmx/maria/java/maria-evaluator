/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.evaluator;

import net.sourceforge.jeval.function.FunctionException;

import java.util.List;

/**
 * @author Osvaldo Miguel Colin
 */
public abstract class MariaEvaluatorFunction {

    private final String name;

    public MariaEvaluatorFunction(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public abstract Object apply(List<Object> args) throws FunctionException;

}
